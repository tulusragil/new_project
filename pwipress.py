from scrapy.contrib.spiders import CrawlSpider
from scrapy.item import Item, Field
from elasticsearch import Elasticsearch
from scrapy.http import Request
import requests
import json

class ItemBerita(Item):
  nomor = Field()
  nama = Field()
  tanggal_lahir = Field()
  jenis_kelamin = Field()
  pendidikan = Field()
  media = Field()
  nomor_anggota = Field()
  status_anggota = Field()
  nomor_kompetensi = Field()
  terdaftar = Field()
  berlaku = Field()
  status = Field()
  media = Field()
  provinsi = Field()
  agama = Field()
  kode = Field()

class pwipressSpiderDetil(CrawlSpider):

  name = 'pwipress'
  url_detil = 'http://pwipress.org/'

  # custom_settings = {
  #       'FEED_URI' : '/home/bino/Desktop/dekstop_backup/kbbi/kbbi/pwipress_6.csv'
  #   }
  
  #1
#   http://pwipress.org/data?page=549
  def start_requests(self):
        for i in range(0,549+1):
            u = "{}data?page={}".format(self.url_detil, i)
            yield Request(
                u,
                callback=self.parse_detil,
            )

  #2
  # def parse_indeks(self, response):
  #   # ambil list url berita dari halaman indeks dulu
  #   # lalu panggil method parse_detil untuk masing2 url 

  #   url = response.xpath('//div[@class="content"]/a/@href').extract()
  #   for i in url:
  #      print 'Link utk diambil:', i
  #      yield Request(url=i, callback=self.parse_detil)



  def parse_detil(self, response):

      #Extract article information
      nomor = response.xpath("//td[@class='views-field views-field-counter']//text()").extract()
      nama = response.xpath("//td[@class='views-field views-field-title']/a//text()").extract()
      tgl_lhir = response.xpath("//td[@class='views-field views-field-field-tanggal-lahir']/span[@class='date-display-single']//text()").extract()
      jns_klmn = response.xpath("//td[@class='views-field views-field-field-jenis-kelamin']//text()").extract()
      pndidikan = response.xpath("//td[@class='views-field views-field-field-pendidikan']//text()").extract()
      media = response.xpath("//td[@class='views-field views-field-field-media']//text()").extract()
      no_agt = response.xpath("//td[@class='views-field views-field-field-nomor-anggota']//text()").extract()
      stats_agt = response.xpath("//td[@class='views-field views-field-field-status-anggota']//text()").extract()
      no_kmptsi = response.xpath("//td[@class='views-field views-field-field-nomor-kompetensi']//text()").extract()
      terdaftar = response.xpath("//td[@class='views-field views-field-field-terdaftar']/span[@class='date-display-single']//text()").extract()
      berlaku = response.xpath("//td[@class='views-field views-field-field-berlaku2']/span[@class='date-display-single']//text()").extract()
      status = response.xpath("//td[@class='views-field views-field-field-status']//text()").extract()
      type_media = response.xpath("//td[@class='views-field views-field-field-media2']//text()").extract()
      prvnsi = response.xpath("//td[@class='views-field views-field-type']//text()").extract()
      agama = response.xpath("//td[@class='views-field views-field-field-agama']//text()").extract()
      kode = response.xpath("//td[@class='views-field views-field-field-kodeangka']//text()").extract()
      
      es = Elasticsearch([{'host':'localhost','port':9200}])

      # if response.status_code=='200':
      for b,c,d,e,f,g,h,i,j,k,l,m,n,o,p  in zip(nama,tgl_lhir,jns_klmn,pndidikan,media,no_agt,stats_agt,terdaftar,berlaku,no_kmptsi,status,type_media,prvnsi,agama,kode):
          b = b
          b = b.replace("'","").replace('"',"").replace("\n","")
          b = b.strip()
          c = c
          c = c.replace("'","").replace('"',"").replace("\n","")
          c = c.strip()
          d = d
          d = d.replace("'","").replace('"',"").replace("\n","")
          d = d.strip()
          e = e
          e = e.replace("'","").replace('"',"").replace("\n","")
          e = e.strip()
          f = f
          f = f.replace("'","").replace('"',"").replace("\n","")
          f = f.strip()
          g = g
          g = g.replace("'","").replace('"',"").replace("\n","")
          g = g.strip()
          h = h
          h = h.replace("'","").replace('"',"").replace("\n","")
          h = h.strip()
          i = i
          i = i.replace("'","").replace('"',"").replace("\n","")
          i = i.strip()
          j = j
          j = j.replace("'","").replace('"',"").replace("\n","")
          j = j.strip()
          k = k
          k = k.replace("'","").replace('"',"").replace("\n","")
          k = k.strip()
          l = l
          l = l.replace("'","").replace('"',"").replace("\n","")
          l = l.strip()
          m = m
          m = m.replace("'","").replace('"',"").replace("\n","")
          m = m.strip()
          n = n
          n = n.replace("'","").replace('"',"").replace("\n","")
          n = n.strip()
          o = o
          o = o.replace("'","").replace('"',"").replace("\n","")
          o = o.strip()
          p = p
          p = p.replace("'","").replace('"',"").replace("\n","")
          p = p.strip()
          # print b
          scraped_info = {
              'nama':b,
              'tanggal_lahir':c,
              'jenis_kelamin':d,
              'pendidikan':e,
              'media':f,
              'nomor_anggota':g,
              'status_anggota':h,
              'nomor_kompetensi':k,
              'terdaftar':j,
              'berlaku':i,
              'status':l,
              'type_media':m,
              'provinsi':n,
              'agama':o,
              'kode':p,
          }
          print scraped_info
          
          es.index(index='pwipress', doc_type='_doc', body=scraped_info)
          yield scraped_info